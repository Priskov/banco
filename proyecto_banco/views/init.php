<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/style.css">

    <title>INICIO</title>
</head>
<body>

<?php
session_start();
if (isset($_SESSION['cliente'])){?>

    <nav class="nav">
        <a href="init.php"> <input name="reg" type="button" value="Init"></a>

        <a href="perfil.php"><input name="ins" type="button" value="Perfil"></a>

        <a href="transfer.php"><input name="out" type="button" value="Transferencias"></a>

        <a href="query.php"><input name="perf" type="button" value="Movimientos"></a>

        <a href="logout.php"><input name="tra" type="button" value="Desconectarse"></a>

    </nav>


    <main>

        <form action="../controller/controller.php" method="post">
            <input name="submit" type="submit" value="Crear cuenta"/>
            <input name="control" type="hidden" value="create"/>
        </form>


        <form action="../controller/controller.php" method="post">
            <select name="cuentas">

                <?php
                require_once('../model/CuentaModel.php');
                require_once('../model/Cliente.php');
                session_start();
                $accounts=getAccounts(unserialize($_SESSION['cliente'])->getDni());
                for ($i=0; $i<sizeof($accounts) ;$i++){?>
                    <option ><?php echo $accounts[$i]["cuenta"] ?></option>
                <?php }?>
            </select>
            <input name="submit" type="submit" value="Seleccionar"/>
            <input name="control" type="hidden" value="select_account"/>
        </form>

    </main>


    <?php
}else{
    header("Location: login.php");

}?>


</body>
</html>
